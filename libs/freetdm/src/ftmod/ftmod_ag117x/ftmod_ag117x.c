/*
 * Copyright (c) 2007-2014, Anthony Minessale II
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * * Neither the name of the original author; nor the names of any contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 * 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "private/ftdm_core.h"
#include <alsa/asoundlib.h>
#include <sys/epoll.h>
#include <limits.h>
//#include "ftdm_ag117x.h"

#define BYTES_PER_FRAME 2


/**
 * \brief AG117x globals
 */
static struct {
    uint32_t sample_rate;
    int codec_ms;
} ag117x_globals;

struct ag117x_span {
	struct pollfd pfds[FTDM_MAX_CHANNELS_SPAN];
};
typedef struct ag117x_span ag117x_span_t;

struct ag117x_channel {
	char *device_name;
	snd_pcm_t *audio_stream_in;
	struct pollfd *audio_stream_in_pfds;
	int audio_stream_in_pfds_count;
	snd_pcm_t *audio_stream_out;
	struct pollfd *audio_stream_out_pfds;
	int audio_stream_out_pfds_count;
	int shkfd, shk_gpio_num;
	int frfd, fr_gpio_num;
	int rmfd, rm_gpio_num;
	ftdm_time_t last_shk_edge;
	ftdm_timer_id_t gpio_debounce_timer;
	struct pollfd *pfds;
};
typedef struct ag117x_channel ag117x_channel_t;

static void *ag1171_genring_run(ftdm_thread_t *me, void *obj);

/**
 * \brief Process configuration variable for a AG117x profile
 * \param category AG117x profile name
 * \param var Variable name
 * \param val Variable value
 * \param lineno Line number from configuration file
 * \return Success
 */
static FIO_CONFIGURE_FUNCTION(ag117x_configure)
{
	int num;

	ftdm_log(FTDM_LOG_DEBUG, "ag117x config variable. category: %s, var: %s, val: %s, lineno: %d", category, var, val, lineno);
	if (!strcasecmp(category, "defaults")) {
		if (!strcasecmp(var, "codec_ms")) {
			num = atoi(val);
			if (num < 10 || num > 60) {
				ftdm_log(FTDM_LOG_WARNING, "invalid codec ms at line %d\n", lineno);
			} else {
				ag117x_globals.codec_ms = num;
			}
		} else if (!strcasecmp(var, "sample_rate")) {
			num = atoi(val);
			if (num < 8000 || num > 44100) {
				ftdm_log(FTDM_LOG_WARNING, "invalid sample rate at line %d\n", lineno);
			} else {
				ag117x_globals.sample_rate = num;
			}
		} else {
			ftdm_log(FTDM_LOG_WARNING, "Ignoring unknown setting '%s'\n", var);
		}
	}


	return FTDM_SUCCESS;
}

/**
 * \brief Initialises a freetdm AG117x span from a configuration string
 * \param span FreeTDM span
 * \param str Configuration string
 * \param type FreeTDM span type
 * \param name FreeTDM span name
 * \param number FreeTDM span number
 * \return Success or failure
 */
static FIO_CONFIGURE_SPAN_FUNCTION(ag117x_configure_span)
{
	int items, i, j, gpio_num, elems, fd = -1, flags, err, k = 0;
	char *mydata, *item_list[10], *elem_list[4], buf[64];
	unsigned configured = 0;
	ftdm_channel_t *ftdmchan;
	ag117x_channel_t *chan_data = NULL;
	ag117x_span_t *span_data = NULL;
	ftdm_socket_t sockfd = -1;
	snd_pcm_hw_params_t *hw_params = NULL;
	snd_pcm_sw_params_t *sw_params = NULL;
	snd_pcm_uframes_t period_size = ag117x_globals.codec_ms*(ag117x_globals.sample_rate/1000);
	snd_pcm_uframes_t buffer_size = period_size*16;
	unsigned int period_time = ag117x_globals.codec_ms * 1000;

	assert(str != NULL);


	mydata = ftdm_strdup(str);
	assert(mydata != NULL);

	//ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "ag117x span config. str: %s, name: %s, number: %s\n",mydata, name, number);

	span->io_data = span_data = ftdm_calloc(1, sizeof(ag117x_span_t));

	items = ftdm_separate_string(mydata, ';', item_list, (sizeof(item_list) / sizeof(item_list[0])));

	for(i = 0; i < items; i++) {
		elems = ftdm_separate_string(item_list[i], '#', elem_list, (sizeof(elem_list) / sizeof(elem_list[0])));
		if (elems < 1 || !elem_list[0]) {
			ftdm_log(FTDM_LOG_ERROR, "Invalid input\n");
			continue;
		}

		if (ftdm_span_add_channel(span, sockfd, type, &ftdmchan) == FTDM_SUCCESS) {
			ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "created channel in span: %s\n",name);
			chan_data = ftdm_calloc(1, sizeof(ag117x_channel_t));
			ftdm_assert(chan_data != NULL, "chan_data alloc failed\n");
			chan_data->device_name = ftdm_strdup(item_list[0]);
			chan_data->shkfd = chan_data->frfd = chan_data->rmfd = -1;
			ftdmchan->io_data = chan_data;
			ftdmchan->physical_chan_id = i;

			if (!ftdm_strlen_zero(name)) {
				ftdm_copy_string(ftdmchan->chan_name, name, sizeof(ftdmchan->chan_name));
			}

			if (!ftdm_strlen_zero(number)) {
				ftdm_copy_string(ftdmchan->chan_number, number, sizeof(ftdmchan->chan_number));
			}

			if (elems >= 2 && elem_list[1]) {
				if ((gpio_num = strtol(elem_list[1], NULL, 10)) > 0) {
					chan_data->shk_gpio_num = gpio_num;
					ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "ag117x span %d SHK GPIO number: %d\n",i, gpio_num);

					if ((fd = open("/sys/class/gpio/export", O_WRONLY)) <= 0) {
						continue;
					}

					sprintf(buf, "%d", chan_data->shk_gpio_num);
					write(fd, buf, strlen(buf));
					close(fd);fd = -1;

					sprintf(buf, "/sys/class/gpio/gpio%d/direction", chan_data->shk_gpio_num);
					if ((fd = open(buf, O_WRONLY)) <= 0) {
						continue;
					}
					if (write(fd, "in", 2) < 0) {
						continue;
					}
					close(fd);fd = -1;

					sprintf(buf, "/sys/class/gpio/gpio%d/edge", chan_data->shk_gpio_num);
					if ((fd = open(buf, O_WRONLY)) <= 0) {
						continue;
					}
					if (write(fd, "both", 4) < 0) {
						continue;
					}
					close(fd);fd = -1;

					sprintf(buf, "/sys/class/gpio/gpio%d/value", chan_data->shk_gpio_num);
					if ((fd = open(buf, O_RDONLY)) <= 0) {
						continue;
					}
					if ((flags = fcntl(fd, F_GETFL, 0)) < 0) {
						continue;
					}
					if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) < 0) {
						continue;
					}
					chan_data->shkfd = fd;
					fd = -1;
				}
			}

			if (elems >= 3 && elem_list[2]) {
				if ((gpio_num = strtol(elem_list[2], NULL, 10)) > 0) {
					chan_data->fr_gpio_num = gpio_num;
					ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "ag117x span %d FR GPIO number: %d\n",i, gpio_num);

					if ((fd = open("/sys/class/gpio/export", O_WRONLY)) <= 0) {
						continue;
					}
					sprintf(buf, "%d", chan_data->fr_gpio_num);
					write(fd, buf, strlen(buf));
					close(fd);fd = -1;

					sprintf(buf, "/sys/class/gpio/gpio%d/direction", chan_data->fr_gpio_num);
					if ((fd = open(buf, O_WRONLY)) <= 0) {
						continue;
					}
					if (write(fd, "out", 3) < 0) {
						continue;
					}
					close(fd);fd = -1;

					sprintf(buf, "/sys/class/gpio/gpio%d/value", chan_data->shk_gpio_num);
					if ((fd = open(buf, O_WRONLY)) <= 0) {
						continue;
					}
					chan_data->frfd = fd;
					fd = -1;
				}
			}

			if (elems >= 4 && elem_list[3]) {
				if ((gpio_num = strtol(elem_list[3], NULL, 10)) > 0) {
					chan_data->rm_gpio_num = gpio_num;
					ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "ag117x span %d RM GPIO number: %d\n",i, gpio_num);

					if ((fd = open("/sys/class/gpio/export", O_WRONLY)) <= 0) {
						continue;
					}
					sprintf(buf, "%d", chan_data->rm_gpio_num);
					write(fd, buf, strlen(buf));
					close(fd);fd = -1;

					sprintf(buf, "/sys/class/gpio/gpio%d/direction", chan_data->rm_gpio_num);
					if ((fd = open(buf, O_WRONLY)) <= 0) {
						continue;
					}
					if (write(fd, "out", 4) < 0) {
						continue;
					}
					close(fd);fd = -1;

					sprintf(buf, "/sys/class/gpio/gpio%d/value", chan_data->shk_gpio_num);
					if ((fd = open(buf, O_WRONLY)) <= 0) {
						continue;
					}
					chan_data->rmfd = fd;
					fd = -1;
				}
			}

			if ((err = snd_pcm_open(&(chan_data->audio_stream_out), chan_data->device_name, SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot open playback audio device %s (%s)\n",chan_data->device_name, snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot allocate hardware parameter structure (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_any(chan_data->audio_stream_out, hw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot initialize hardware parameter structure (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_access(chan_data->audio_stream_out, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set access type (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_format(chan_data->audio_stream_out, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set sample format (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_rate(chan_data->audio_stream_out, hw_params, ag117x_globals.sample_rate, 0)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set sample rate (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_channels(chan_data->audio_stream_out, hw_params, 1)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set channel count (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_period_time_near(chan_data->audio_stream_out, hw_params, &period_time, 0)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set period time %du (%s)\n", period_time, snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_get_period_size(hw_params, &period_size, 0)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot get period size %du (%s)\n", period_time, snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_buffer_size(chan_data->audio_stream_out, hw_params, buffer_size)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set buffer size (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params(chan_data->audio_stream_out, hw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set parameters (%s)\n", snd_strerror(err));
				goto fail;
			}

			snd_pcm_hw_params_free(hw_params);
			hw_params = NULL;

			if ((err = snd_pcm_sw_params_malloc(&sw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot allocate software parameter structure (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_sw_params_current(chan_data->audio_stream_out, sw_params)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params_set_start_threshold(chan_data->audio_stream_out, sw_params, ULONG_MAX)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params_set_stop_threshold(chan_data->audio_stream_out, sw_params, ULONG_MAX)) < 0) {
				goto fail;
			}


			if ((err = snd_pcm_sw_params_set_avail_min(chan_data->audio_stream_out, sw_params, period_size*2)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params_set_silence_size(chan_data->audio_stream_out, sw_params, period_size*2)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params_set_silence_threshold(chan_data->audio_stream_out, sw_params, period_size*2)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params(chan_data->audio_stream_out, sw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set parameters (%s)\n", snd_strerror(err));
				goto fail;
			}

			if (sw_params) {
				snd_pcm_sw_params_free(sw_params);
				sw_params = NULL;
			}

			if ((err = snd_pcm_open(&(chan_data->audio_stream_in), chan_data->device_name, SND_PCM_STREAM_CAPTURE, SND_PCM_NONBLOCK)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot open capture audio device %s (%s)\n",chan_data->device_name, snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot allocate hardware parameter structure (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_any(chan_data->audio_stream_in, hw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot initialize hardware parameter structure (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_access(chan_data->audio_stream_in, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set access type (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_format(chan_data->audio_stream_in, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set sample format (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_rate(chan_data->audio_stream_in, hw_params, ag117x_globals.sample_rate, 0)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set sample rate (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_channels(chan_data->audio_stream_in, hw_params, 1)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set channel count (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_period_time(chan_data->audio_stream_in, hw_params, period_time, 0)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set period time %du (%s)\n", period_time, snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params_set_buffer_size(chan_data->audio_stream_in, hw_params, buffer_size)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set buffer size (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_hw_params(chan_data->audio_stream_in, hw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set parameters (%s)\n", snd_strerror(err));
				goto fail;
			}

			if (hw_params) {
				snd_pcm_hw_params_free(hw_params);
				hw_params = NULL;
			}

			if ((err = snd_pcm_sw_params_malloc(&sw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot allocate software parameter structure (%s)\n", snd_strerror(err));
				goto fail;
			}

			if ((err = snd_pcm_sw_params_current(chan_data->audio_stream_in, sw_params)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params_set_start_threshold(chan_data->audio_stream_in, sw_params, ULONG_MAX)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params_set_stop_threshold(chan_data->audio_stream_in, sw_params, ULONG_MAX)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params_set_avail_min(chan_data->audio_stream_in, sw_params, period_size*2)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params_set_silence_size(chan_data->audio_stream_in, sw_params, period_size*2)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params_set_silence_threshold(chan_data->audio_stream_in, sw_params, period_size*2)) < 0) {
				goto fail;
			}

			if ((err = snd_pcm_sw_params(chan_data->audio_stream_in, sw_params)) < 0) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot set parameters (%s)\n", snd_strerror(err));
				goto fail;
			}

			if (sw_params) {
				snd_pcm_sw_params_free(sw_params);
				sw_params = NULL;
			}


			chan_data->audio_stream_out_pfds_count = snd_pcm_poll_descriptors_count(chan_data->audio_stream_out);
			chan_data->audio_stream_out_pfds = ftdm_malloc(chan_data->audio_stream_out_pfds_count*sizeof(struct pollfd));
			if (snd_pcm_poll_descriptors(chan_data->audio_stream_out, chan_data->audio_stream_out_pfds, chan_data->audio_stream_out_pfds_count) < 0) {
				goto fail;
			}

			chan_data->audio_stream_in_pfds_count = snd_pcm_poll_descriptors_count(chan_data->audio_stream_in);
			chan_data->audio_stream_in_pfds = ftdm_malloc(chan_data->audio_stream_in_pfds_count*sizeof(struct pollfd));
			if (snd_pcm_poll_descriptors(chan_data->audio_stream_in, chan_data->audio_stream_in_pfds, chan_data->audio_stream_in_pfds_count) < 0) {
				goto fail;
			}

			chan_data->pfds = ftdm_calloc(chan_data->audio_stream_in_pfds_count+chan_data->audio_stream_in_pfds_count, sizeof(struct pollfd));

			for(j=0; j < chan_data->audio_stream_out_pfds_count; ++j) {
				chan_data->pfds[k++] = chan_data->audio_stream_out_pfds[j];
			}
			for(j=0; j < chan_data->audio_stream_in_pfds_count; ++j) {
				chan_data->pfds[k++] = chan_data->audio_stream_in_pfds[j];
			}

			ftdmchan->native_codec = ftdmchan->effective_codec = FTDM_CODEC_SLIN;
			ftdmchan->native_interval = ftdmchan->effective_interval = period_time/1000;
			ftdmchan->packet_len = period_size*2;

			ftdm_thread_create_detached(ag1171_genring_run, ftdmchan);

			configured += 1;
			continue;
			fail:
			if (hw_params) {
				snd_pcm_hw_params_free(hw_params);
				hw_params = NULL;
			}

			if (sw_params) {
				snd_pcm_sw_params_free(sw_params);
				sw_params = NULL;
			}

			if (chan_data->audio_stream_in) {
				snd_pcm_close(chan_data->audio_stream_in);
				chan_data->audio_stream_in = NULL;
			}

			if (chan_data->audio_stream_out) {
				snd_pcm_close(chan_data->audio_stream_out);
				chan_data->audio_stream_out = NULL;
			}

			if (chan_data->pfds) {
				ftdm_safe_free(chan_data->pfds);
				chan_data->pfds = NULL;
			}

			if (chan_data->audio_stream_out_pfds) {
				ftdm_safe_free(chan_data->audio_stream_out_pfds);
				chan_data->audio_stream_out_pfds_count = 0;
			}

			if (chan_data->audio_stream_in_pfds) {
				ftdm_safe_free(chan_data->audio_stream_in_pfds);
				chan_data->audio_stream_in_pfds_count = 0;
			}
		}

	}

	for(i = 1; i <= span->chan_count; i++) {
		ftdmchan = span->channels[i];
		chan_data = ftdmchan->io_data;
		span_data->pfds[i-1].fd = chan_data->shkfd;
		span_data->pfds[i-1].events = POLLERR|POLLPRI;
	}


	ftdm_safe_free(mydata);

	return configured;

}

/**
 * \brief Opens AG117x channel
 * \param ftdmchan Channel to open
 * \return Success or failure
 */
static FIO_OPEN_FUNCTION(ag117x_open)
{
	int err = 0;
	ag117x_channel_t *chan_data;

	ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "ag117x opening channel in %d\n",ftdmchan->chan_id);

	if (ftdmchan->type != FTDM_CHAN_TYPE_FXS) {
		return FTDM_FAIL;
	}

	ftdm_channel_lock(ftdmchan);

	chan_data = ftdmchan->io_data;

	if ((err = snd_pcm_prepare(chan_data->audio_stream_out)) < 0) {
		ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot prepare playback audio interface for use (%s)\n", snd_strerror(err));
		goto fail;
	}

	if ((err = snd_pcm_prepare(chan_data->audio_stream_in)) < 0) {
		ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot prepare capture audio interface for use (%s)\n", snd_strerror(err));
		goto fail;
	}

	if ((err = snd_pcm_start(chan_data->audio_stream_out)) < 0) {
		ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot start playback audio interface for use (%s)\n", snd_strerror(err));
		goto fail;
	}

	if ((err = snd_pcm_start(chan_data->audio_stream_in)) < 0) {
		ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "cannot start capture audio interface for use (%s)\n", snd_strerror(err));
		goto fail;
	}

	ftdm_channel_unlock(ftdmchan);

	return FTDM_SUCCESS;
	fail:

	ftdm_channel_unlock(ftdmchan);

	return FTDM_FAIL;
}

/**
 * \brief Closes AG117x channel
 * \param ftdmchan Channel to close
 * \return Success
 */
static FIO_CLOSE_FUNCTION(ag117x_close)
{
	ag117x_channel_t *chan_data;

	ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "ag117x closing channel in %d\n",ftdmchan->chan_id);

	ftdm_channel_lock(ftdmchan);

	chan_data = ftdmchan->io_data;

	if (!chan_data) {
		ftdm_channel_unlock(ftdmchan);
		return FTDM_FAIL;
	}

	if (chan_data->audio_stream_in) {
		snd_pcm_drain(chan_data->audio_stream_in);
		ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "Drain IN stream %s\n", chan_data->device_name);
	}


	if (chan_data->audio_stream_out) {
		snd_pcm_drain(chan_data->audio_stream_out);
		ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "Drain OUT stream %s\n", chan_data->device_name);
	}

	ftdm_channel_unlock(ftdmchan);

	return FTDM_SUCCESS;
}



/**
 * \brief Waits for an event on a AG117x channel
 * \param ftdmchan Channel to open
 * \param flags Type of event to wait for
 * \param to Time to wait (in ms)
 * \return Success, failure or timeout
 */
static FIO_WAIT_FUNCTION(ag117x_wait)
{
	ag117x_channel_t *chan_data = ftdmchan->io_data;
	nfds_t n=0;
	struct pollfd *pfds=NULL;
	int audio_stream_out_pfds_count = 0, audio_stream_in_pfds_count = 0;
	unsigned short revents;


	//ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "ag117x wait event in channel %d. flags: %d, to: %d\n",ftdmchan->chan_id, *flags, to);

	if (*flags & FTDM_WRITE) {
		audio_stream_out_pfds_count = chan_data->audio_stream_out_pfds_count;
		pfds = chan_data->audio_stream_out_pfds;
		n += chan_data->audio_stream_out_pfds_count;
	}
	if (*flags & FTDM_READ) {
		audio_stream_in_pfds_count = chan_data->audio_stream_in_pfds_count;
		if (pfds == NULL) {
			pfds = chan_data->audio_stream_in_pfds;
		}
		n += chan_data->audio_stream_in_pfds_count;
	}

	*flags = FTDM_NO_FLAGS;

	if (poll(pfds, n, to) <= 0) {
		return FTDM_TIMEOUT;
	}

	*flags |= FTDM_EVENTS;

	if (audio_stream_out_pfds_count > 0 && snd_pcm_poll_descriptors_revents(chan_data->audio_stream_out, chan_data->audio_stream_out_pfds, audio_stream_out_pfds_count, &revents) == 0) {
		if (revents|POLLOUT) {
			*flags |= FTDM_WRITE;
		}
	}

	if (audio_stream_in_pfds_count > 0 && snd_pcm_poll_descriptors_revents(chan_data->audio_stream_in, chan_data->audio_stream_in_pfds, audio_stream_in_pfds_count, &revents) == 0) {
		if (revents|POLLIN) {
			*flags |= FTDM_READ;
		}
	}

	return FTDM_SUCCESS;
}

/**
 * \brief Reads data from a AG117x channel
 * \param ftdmchan Channel to read from
 * \param data Data buffer
 * \param datalen Size of data buffer
 * \return Success, failure or timeout
 */
static FIO_READ_FUNCTION(ag117x_read)
{
	int samples = 0;
	ag117x_channel_t *chan_data;
	snd_pcm_uframes_t size = *datalen/BYTES_PER_FRAME;

	chan_data = ftdmchan->io_data;

	if ((samples = snd_pcm_readi(chan_data->audio_stream_in, data, size)) > 0) {
		*datalen = samples*BYTES_PER_FRAME;
		return FTDM_SUCCESS;
	}

	return FTDM_FAIL;
}

/**
 * \brief Writes data to a AG117x channel
 * \param ftdmchan Channel to write to
 * \param data Data buffer
 * \param datalen Size of data buffer
 * \return Success or failure
 */
static FIO_WRITE_FUNCTION(ag117x_write)
{
	ag117x_channel_t *chan_data;
	snd_pcm_uframes_t size = *datalen/BYTES_PER_FRAME;

	chan_data = ftdmchan->io_data;

	*datalen = snd_pcm_writei(chan_data->audio_stream_out, data, size)*BYTES_PER_FRAME;

	return FTDM_SUCCESS;
}

/**
 * \brief Executes an FreeTDM command on a AG117x channel
 * \param ftdmchan Channel to execute command on
 * \param command FreeTDM command to execute
 * \param obj Object (unused)
 * \return Success or failure
 */
static FIO_COMMAND_FUNCTION(ag117x_command)
{
	int err = 0;
	ag117x_channel_t *chan_data;

	ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "ag117x execute command on channel %d. command: %d\n",ftdmchan->chan_id, command);

	ftdm_channel_lock(ftdmchan);

	chan_data = ftdmchan->io_data;

	switch(command) {
	case FTDM_COMMAND_GENERATE_RING_ON:
	{
		ftdm_set_flag_locked(ftdmchan, FTDM_CHANNEL_RINGING);
	}
		break;
	case FTDM_COMMAND_GENERATE_RING_OFF:
	{
		ftdm_clear_flag_locked(ftdmchan, FTDM_CHANNEL_RINGING);
	}
		break;
	case FTDM_COMMAND_SET_POLARITY:
	{
		ftdm_polarity_t polarity = FTDM_COMMAND_OBJ_INT;
		ftdmchan->polarity = polarity;
		if (chan_data->frfd > -1) {
			lseek(chan_data->frfd, 0, SEEK_SET);
			if (polarity == FTDM_POLARITY_FORWARD) {
				write(chan_data->frfd, "1", 1);
			} else {
				write(chan_data->frfd, "0", 1);
			}
		}
	}
		break;
	case FTDM_COMMAND_GET_INTERVAL:
	{
		FTDM_COMMAND_OBJ_INT = ftdmchan->native_interval;

	}
		break;
	default:
		err = FTDM_NOTIMPL;
		break;
	}

	if (err && err != FTDM_NOTIMPL) {
		snprintf(ftdmchan->last_error, sizeof(ftdmchan->last_error), "%s", strerror(errno));
		ftdm_channel_unlock(ftdmchan);
		return FTDM_FAIL;
	}

	ftdm_channel_unlock(ftdmchan);

	return err == 0 ? FTDM_SUCCESS : err;
}

/**
 * \brief Checks for events on a AG117x span
 * \param span Span to check for events
 * \param ms Time to wait for event
 * \return Success if event is waiting or failure if not
 */
static FIO_SPAN_POLL_EVENT_FUNCTION(ag117x_poll_event)
{
	uint32_t i, k = 0;
	int r;
	ftdm_channel_t *ftdmchan;
	ag117x_span_t *span_data = span->io_data;

	ftdm_unused_arg(poll_events);

	r = poll(span_data->pfds, span->chan_count, ms);
	if (r == 0) {
		return FTDM_TIMEOUT;
	} else if (r < 0) {
		snprintf(span->last_error, sizeof(span->last_error), "%s", strerror(errno));
		return FTDM_FAIL;
	}

	for(i = 1; i <= span->chan_count; i++) {
		ftdmchan = span->channels[i];
		ftdm_channel_lock(ftdmchan);

		if (span_data->pfds[i-1].revents & (POLLPRI|POLLERR)) {
			ftdm_sleep(15);
			ftdm_set_io_flag(ftdmchan, FTDM_CHANNEL_IO_EVENT);
			ftdmchan->last_event_time = ftdm_current_time_in_ms();
			++k;
		}

		ftdm_channel_unlock(ftdmchan);

	}

	return k ? FTDM_SUCCESS : FTDM_TIMEOUT;
}

/**
 * \brief Retrieves an event from a AG117x span
 * \param span Span to retrieve event from
 * \param event FreeTDM event to return
 * \return Success or failure
 */
static FIO_SPAN_NEXT_EVENT_FUNCTION(ag117x_next_event)
{
	uint32_t i, event_id = FTDM_OOB_NOOP;
	char value;

	//ftdm_log(FTDM_LOG_DEBUG, "ag117x span %d retrieve event\n",span->span_id);

	for (i = 1; i <= span->chan_count; i++) {
		ftdm_channel_t *ftdmchan = span->channels[i];
		ag117x_channel_t *chan_data;

		ftdm_channel_lock(ftdmchan);

		chan_data = ftdmchan->io_data;

		if (!ftdm_test_io_flag(ftdmchan, FTDM_CHANNEL_IO_EVENT)) {
			ftdm_channel_unlock(ftdmchan);
			continue;
		}

		ftdm_clear_io_flag(ftdmchan, FTDM_CHANNEL_IO_EVENT);

		if (lseek(chan_data->shkfd, 0, SEEK_SET) < 0 || read(chan_data->shkfd, &value, 1) < 0) {
			ftdm_channel_unlock(ftdmchan);
			continue;
		}

		if(value == '0') {
			event_id = FTDM_OOB_ONHOOK;
			ftdm_clear_flag(ftdmchan, FTDM_CHANNEL_OFFHOOK);
		} else {
			event_id = FTDM_OOB_OFFHOOK;
			ftdm_set_flag(ftdmchan, FTDM_CHANNEL_OFFHOOK);
		}

		ftdmchan->last_event_time = 0;
		span->event_header.e_type = FTDM_EVENT_OOB;
		span->event_header.enum_id = event_id;
		span->event_header.channel = ftdmchan;
		*event = &span->event_header;

		ftdm_channel_unlock(ftdmchan);

		return FTDM_SUCCESS;
	}

	return FTDM_FAIL;
}

/**
 * \brief Destroys a AG117x Channel
 * \param ftdmchan Channel to destroy
 * \return Success
 */
static FIO_CHANNEL_DESTROY_FUNCTION(ag117x_channel_destroy)
{
	ag117x_channel_t *chan_data;
	int fd;
	char buf[64];

	ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "ag117x destroy channel %d\n",ftdmchan->chan_id);

	ftdm_channel_lock(ftdmchan);

	chan_data = ftdmchan->io_data;

	if (!chan_data) {
		ftdm_channel_unlock(ftdmchan);
		return FTDM_FAIL;
	}

	if (chan_data->shkfd != -1) {
		close(chan_data->shkfd);
		chan_data->shkfd = -1;
	}
	if (chan_data->frfd != -1) {
		close(chan_data->frfd);
		chan_data->frfd = -1;
	}
	if (chan_data->rmfd != -1) {
		close(chan_data->rmfd);
		chan_data->rmfd = -1;
	}

	if ((fd = open("/sys/class/gpio/unexport", O_WRONLY)) > 0) {
		if (chan_data->shk_gpio_num != -1) {
			sprintf(buf, "%d", chan_data->shk_gpio_num);
			lseek(fd, 0, SEEK_SET);
			write(fd, buf, strlen(buf));
			chan_data->shk_gpio_num = -1;
		}

		if (chan_data->fr_gpio_num != -1) {
			sprintf(buf, "%d", chan_data->fr_gpio_num);
			lseek(fd, 0, SEEK_SET);
			write(fd, buf, strlen(buf));
			chan_data->fr_gpio_num = -1;
		}

		if (chan_data->rm_gpio_num != -1) {
			sprintf(buf, "%d", chan_data->rm_gpio_num);
			lseek(fd, 0, SEEK_SET);
			write(fd, buf, strlen(buf));
			chan_data->rm_gpio_num = -1;
		}

		close(fd);
	}

	ftdm_safe_free(chan_data->device_name);

	if (chan_data->audio_stream_in) {
		snd_pcm_drain(chan_data->audio_stream_in);
		snd_pcm_close(chan_data->audio_stream_in);
		chan_data->audio_stream_in = NULL;
	}

	if (chan_data->audio_stream_out) {
		snd_pcm_drain(chan_data->audio_stream_out);
		snd_pcm_close(chan_data->audio_stream_out);
		chan_data->audio_stream_out = NULL;
	}

	ftdm_safe_free(chan_data->pfds);

	ftdm_safe_free(chan_data->audio_stream_out_pfds);

	ftdm_safe_free(chan_data->audio_stream_in_pfds);

	ftdm_safe_free(chan_data);

	ftdmchan->io_data = NULL;

	ftdm_channel_unlock(ftdmchan);

	return FTDM_SUCCESS;
}

static FIO_SPAN_DESTROY_FUNCTION(ag117x_span_destroy)
{
	ftdm_safe_free(span->io_data);
	return FTDM_SUCCESS;
}

static FIO_GET_ALARMS_FUNCTION(ag117x_get_alarms)
{
	return FTDM_SUCCESS;
}

static void *ag1171_genring_run(ftdm_thread_t *me, void *obj) {
	ftdm_channel_t *ftdmchan = (ftdm_channel_t *) obj;
	ag117x_channel_t *chan_data;
	ftdm_time_t timeend;
	char value[2];
	uint8_t v;
	int frfd, rmfd;
	uint64_t flags;
	ftdm_polarity_t polarity;

	ftdm_unused_arg(me);

	ftdm_channel_lock(ftdmchan);
	chan_data = ftdmchan->io_data;
	frfd = chan_data->frfd;
	rmfd = chan_data->rmfd;
	flags = ftdmchan->flags;
	polarity = ftdmchan->polarity;
	ftdm_channel_unlock(ftdmchan);

	while (ftdm_running() && (flags|FTDM_CHANNEL_INTHREAD)) {
		if (frfd > -1 && rmfd > -1 && (flags|FTDM_CHANNEL_RINGING)) {
			v = (polarity == FTDM_POLARITY_FORWARD) ? 1 : 0;
			lseek(rmfd, 0, SEEK_SET);
			write(rmfd, "1", 1);
			timeend = ftdm_current_time_in_ms() + 3000;
			while (ftdm_current_time_in_ms() < timeend && ftdm_running() && (flags|FTDM_CHANNEL_INTHREAD)) {
				v ^= 1;
				sprintf(value, "%d", v);
				lseek(frfd, 0, SEEK_SET);
				write(frfd, value, 1);
				ftdm_sleep(20);
				ftdm_channel_lock(ftdmchan);
				flags = ftdmchan->flags;
				ftdm_channel_unlock(ftdmchan);
			}
			lseek(rmfd, 0, SEEK_SET);
			write(rmfd, "0", 1);
			ftdm_sleep(4000);
		} else {
			if (rmfd > -1) {
				lseek(rmfd, 0, SEEK_SET);
				write(rmfd, "0", 1);
			}
			ftdm_sleep(100);
		}
		ftdm_channel_lock(ftdmchan);
		flags = ftdmchan->flags;
		polarity = ftdmchan->polarity;
		ftdm_channel_unlock(ftdmchan);
	}

	return NULL;
}

static ftdm_io_interface_t ag117x_interface;

/**
 * \brief Loads AG117x IO module
 * \param fio FreeTDM IO interface
 * \return Success
 */
static FIO_IO_LOAD_FUNCTION(ag117x_init)
{
	assert(fio != NULL);
	memset(&ag117x_interface, 0, sizeof(ag117x_interface));
	memset(&ag117x_globals, 0, sizeof(ag117x_globals));

	ag117x_globals.codec_ms = 20;
	ag117x_globals.sample_rate = 8000;
	ag117x_interface.name = "ag117x";
	ag117x_interface.configure =  ag117x_configure;
	ag117x_interface.configure_span =  ag117x_configure_span;
	ag117x_interface.open = ag117x_open;
	ag117x_interface.close = ag117x_close;
	ag117x_interface.wait = ag117x_wait;
	ag117x_interface.read = ag117x_read;
	ag117x_interface.write = ag117x_write;
	ag117x_interface.command = ag117x_command;
	ag117x_interface.poll_event = ag117x_poll_event;
	ag117x_interface.next_event = ag117x_next_event;
	ag117x_interface.channel_destroy = ag117x_channel_destroy;
	ag117x_interface.span_destroy = ag117x_span_destroy;
	ag117x_interface.get_alarms = ag117x_get_alarms;
	*fio = &ag117x_interface;

	return FTDM_SUCCESS;
}

/**
 * \brief Unloads AG117x IO module
 * \return Success
 */
static FIO_IO_UNLOAD_FUNCTION(ag117x_destroy)
{
	memset(&ag117x_interface, 0, sizeof(ag117x_interface));
	memset(&ag117x_globals, 0, sizeof(ag117x_globals));
	return FTDM_SUCCESS;
}


ftdm_module_t ftdm_module = { 
	"ag117x",
	ag117x_init,
	ag117x_destroy,
};


/* For Emacs:
 * Local Variables:
 * mode:c
 * indent-tabs-mode:t
 * tab-width:4
 * c-basic-offset:4
 * End:
 * For VIM:
 * vim:set softtabstop=4 shiftwidth=4 tabstop=4 noet:
 */

